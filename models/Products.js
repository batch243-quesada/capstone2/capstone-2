const mongoose = require('mongoose');

const productSchema = new mongoose.Schema(
		{
			title: { type: String, required: true, unique: true },
			description: { type: String, required: true },
			// img: { type: String, required: true },
			categories: { type: String, default: "Single-split type" },
			unit: { type: String, required: true},
			hp: { type: Number, required: true },
			price: { type: Number, required: true },
			stocks: { type: Number, required: true },
			isActive: { type: Boolean, default: true }
		},
		{
			timestamps: true
		}
	)

module.exports = mongoose.model("Product", productSchema);
